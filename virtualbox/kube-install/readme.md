# Kube-Install lab
## A pre-built VM cluster you can practice installing Kubernetes on.
### Derived from https://github.com/kodekloudhub/certified-kubernetes-administrator-course

### How to use:
 - CD to the kube-install folder
 - `vagrant up` to bring up the cluster
 - `vagrant ssh` to get to the control plane and nodes
 - `vagrant destroy` to destroy the cluster

### Configuration:
 - One control plane node (`kubecontrolplane`) with 2GB RAM and 2 CPU cores
 - 2 worker nodes (`kubeworker01` and `kubeworker02`) with 1 GB ram and 2 CPU cores
 - Docker installed, swap is off, iptables is bridged( there are separate script files to do each task, you can adjust if you wish)
 - DNS is set up and etc/hosts is set up ( all three nodes will communicate to each other)
 - configuration can be adjusted by making changes to the vagrantfile and supporting update-dns file.

## Things do do:
 - Go through the kubernetes documentation and practice installing the cluster. Basic steps include:
  - Install kubeadm(https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
  - Install a CNI. Weave is suggested here, all we are practicing is installing the cluster. `kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"`
  - Configure/init kubeadm Hint: when you call `kubeadm init` be sure to pass `--apiserver-advertise-address=192.168.56.2`( IP of the control plane node) or else the cluster won't work.
