OUTPUT_FILE=/vagrant/join.sh

sudo kubeadm init --apiserver-advertise-address=192.168.56.2 --pod-network-cidr=10.244.0.0/16 | grep -Ei "kubeadm join|discovery-token-ca-cert-hash" > ${OUTPUT_FILE}
sudo chmod +x $OUTPUT_FILE

# add the kubeconfig to the vagrant and root home directories.
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
sudo mkdir -p /root/.kube
sudo cp -i /etc/kubernetes/admin.conf /root/.kube/config
sudo chown -R root:root /root/.kube

# Fix kubelet IP - we may not need this?
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=192.168.56.2"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

#install weave
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
