# cluster upgrade lab

### How to use:
 - CD to the cluster-upgrade folder
 - `vagrant up` to bring up the cluster
 - `vagrant ssh` to get to the control plane and nodes
 - `vagrant destroy` to destroy the cluster

### Configuration:
 - One control plane node (`upgradelabcontrolplane`) with 2GB RAM and 2 CPU cores
 - 2 worker nodes (`upgradelabworker01` and `upgradelabworker02`) with 1 GB ram and 2 CPU cores
 - Kubernetes is fully set up so you can concentrate on cluster upgrades. Kubernetes is at v 1.20.0

## Things do do:
 - go through the kubernetes documentation and practice upgrading kubernetes to v 1.21.0
