# etcd backup and restore lab

### How to use:
 - CD to the etc-backup-restore folder
 - `vagrant up` to bring up the cluster
 - `vagrant ssh` to get to the control plane and nodes
 - `vagrant destroy` to destroy the cluster

### Configuration:
 - One control plane node (`etcdlabcontrolplane`) with 2GB RAM and 2 CPU cores
 - 2 worker nodes (`etclabworker01` and `etcdlabworker02`) with 1 GB ram and 2 CPU cores
 - Kubernetes is fully set up so you can concentrate on etcd backup and restore

## Things do do:
 - go through the kubernetes documentation and practice backing up and restoring etcd.
