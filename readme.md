# Broken-Clusters

## Broken kubernetes clusters for you to spin up and try to fix.

## UPDATE 9/2/2023: This repo is no longer maintained and just here for posterity. Feel free to fork and make changes as you wish, however i'm probably not going to be taking merge requests on this repo or providing any support. Get it while it's here, i may be removing this by end of year. ALSO: please note the troubleshooting scenario is not present. I passed the CKA before i could add this, but you can easily add a scenario like this by fiddling with the scripts that install kubernetes in the "ubuntu" directory. 


## Purpose:
  - I am studying for CKA exam and i created several vagrant files to spin up kubernetes clusters to practice the following:
   - etcd backup and restore
   - Kubernetes installation the kubeadm way( the way you would do it on the exam)
   - Upgrading a cluster

## Folder list:
 - virtualbox folder: corresponding VM environments in VirtualBox (VMware possibly after my exam when i have more time)
 - etc-backup-restore : setup for etcd backup and restore. Kubernetes is installed on the cluster.
 - kube-install: setup for installing kubernetes: a simple 3 node cluster ready to install kubernetes on it
 - cluster-upgrade: set up for upgrading kubernetes. Kubernetes 1.20.0 is installed.

## What you need:
 - Vagrant : https://www.vagrantup.com/
 - VirtualBox(https://www.virtualbox.org/)
 - You will need 4GB of RAM and 8 cores free to run the files. The control plane will use 2 cores and 2 GB of RAM, each worker will take 2 cores and 1 GB ram each.
 - Basic knowledge of vagrantfiles. You don't need to be an expert but you will need to at least be able to alter the files for your needs(if you need to adjust memory/cpu, ip addresses, etc.)
 - Linux and bash scripting knowledge : again, not much is needed, just enough to decipher the scripts that break things so you can learn more about it.


 ### Usage:
 - In each folder, you will find a vagrant file. CD to the folder with the vagrant file in it and run `vagrant up`.
 - Once the cluster is up, use `vagrant ssh` to get into the control plane or worker nodes.
 - When you are done with the cluster, you can `vagrant destroy` to get rid of it.

 ### This would not be possible without the following folks:

  - KodeKloud and Mumshad Mannambeth: most of the vagrantfile base is taken from https://github.com/kodekloudhub/certified-kubernetes-administrator-course . his Udemy course is a must get for training! check it out at https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/ and check out https://kodekloud.com
  - Anthony Nocentino's classes on Pluralsight: His troubleshooting course is very good and i got the idea of breaking the clusters from him.
